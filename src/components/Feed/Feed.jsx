import Post from "../Post/Post";
import Share from "../Share/Share";
import "./Feed.css";

import { Posts } from "../../dummyData";

const Feed = () => {
  console.log(Posts);
  return (
    <div className="feed">
      <div className="feedWrapper">
        <Share />

        {Posts?.map((post) => (
          <Post
            key={post.id}
            desc={post.desc}
            img={post.photo}
            like={post.like}
            comment={post.comment}
            data={post.date}
            id={post.id}
          />
        ))}
      </div>
    </div>
  );
};

export default Feed;
