import "./Following.css";

const Following = ({ profilePicture, username }) => {
  return (
    <div className="rightbarFollowing">
      <img className="rightbarFollowingImg" src={profilePicture} alt="" />
      <span className="rightbarFollowingName">{username}</span>
    </div>
  );
};

export default Following;
