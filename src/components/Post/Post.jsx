import "./Post.css";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import { Users } from "../../dummyData";
import { useState } from "react";
const Post = ({ desc, img, date, like, comment, id }) => {
  const [postLike, setPostLike] = useState(like);
  const [isLiked, setIsLike] = useState(false);

  const likeHandler = () => {
    setPostLike(isLiked ? postLike - 1 : postLike + 1);
    setIsLike(!isLiked);
  };

  return (
    <div className="post">
      <div className="postWrapper">
        <div className="postTop">
          <div className="postTopLeft">
            <img
              className="postProfileImg"
              src={Users.filter((user) => user.id === id)[0].profilePicture}
              alt=""
            />
            <span className="postUsername">{Users.filter((user) => user.id === id)[0].username}</span>
            <span className="postDate">{date}</span>
          </div>
          <div className="postTopRight">
            <MoreVertIcon />
          </div>
        </div>
        <div className="postCenter">
          <span className="postText">{desc}</span>
          <img className="postImg" src={img} alt="" />
        </div>
        <div className="postBottom">
          <div className="postBottomLeft">
            <img onClick={likeHandler} className="likeIcon" src="assets/like.png" alt="" />
            <img onClick={likeHandler} className="likeIcon" src="assets/heart.png" alt="" />
            <span className="postLikeCounter">{postLike}</span>
          </div>
          <div className="postBottomRight">
            <span className="postCommentText">{comment}</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Post;
