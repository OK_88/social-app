import "./RegisterPage.css";

const RegisterPage = () => {
  return (
    <div className="login">
      <div className="loginWrapper">
        <div className="loginLeft">
          <h3 className="loginLogo">LOGO</h3>
          <span className="loginDesc">Connect with friends and the world around you on Logo</span>
        </div>
        <div className="loginRight">
          <div className="loginBox">
            <input placeholder="User name" className="loginInput" />
            <input placeholder="Email" className="loginInput" />
            <input placeholder="Password" className="loginInput" />
            <input placeholder="Passowrd Again" className="loginInput" />
            <button className="loginButton">Sign Up</button>
            <button className="loginRegisterButton">Log into Account</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RegisterPage;
